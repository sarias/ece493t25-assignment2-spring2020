import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, reward_decay=0.9, e_greedy=0.1, learning_rate=0.01):
        self.actions = actions  
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.alpha = learning_rate
        self.q_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Q-Learning"

    def choose_action(self, observation):
        self.check_state_exist(observation)
        # There's a probability of epsilon that the agent will choose a random action
        if np.random.uniform() <= self.epsilon:
            # Choose a random action available at current state
            action = np.random.choice(self.actions)
        else:
            state_action = self.q_table.loc[observation, :]
            # Choose the action that has the greatest Q value
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        return action

    def learn(self, s, a, r, s_prime):
        self.check_state_exist(s_prime)

        if s_prime != 'terminal':
            state_prime_action = self.q_table.loc[s_prime, :]
            max_action = np.random.choice(state_prime_action[state_prime_action == np.max(state_prime_action)].index)
            old_q = self.q_table.loc[s, a]
            new_q = old_q + self.alpha*(r + (self.gamma*self.q_table.loc[s_prime, max_action]) - old_q)
        else:
            new_q = r  # next state is terminal
        
        self.q_table.loc[s, a] = new_q  # update
        a_prime = self.choose_action(s_prime)
        return s_prime, a_prime

    def check_state_exist(self, state):
        if state not in self.q_table.index:
            # append new state to q table
            self.q_table = self.q_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q_table.columns,
                    name=state,
                )
            )