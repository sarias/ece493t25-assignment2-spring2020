import numpy as np
import pandas as pd


class rlalgorithm:
    def __init__(self, actions, reward_decay=0.9, e_greedy=0.1, learning_rate=0.01):
        self.actions = actions  
        self.gamma = reward_decay
        self.epsilon = e_greedy
        self.alpha = learning_rate
        self.q1_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.q2_table = pd.DataFrame(columns=self.actions, dtype=np.float64)
        self.display_name="Double Q-Learning"

    def choose_action(self, observation):
        self.check_state_exist(observation)
        # There's a probability of epsilon that the agent will choose a random action
        if np.random.uniform() <= self.epsilon:
            # Choose a random action available at current state
            action = np.random.choice(self.actions)
        else:
            state_action_q1 = self.q1_table.loc[observation, :]
            state_action_q2 = self.q2_table.loc[observation, :]
            state_action = state_action_q1.add(state_action_q2, fill_value=0)
            # Choose the action that has the greatest Q value
            action = np.random.choice(state_action[state_action == np.max(state_action)].index)
        return action

    def learn(self, s, a, r, s_prime):
        self.check_state_exist(s_prime)  

        if np.random.uniform() <= 0.5:
            if s_prime != 'terminal':
                q1_state_prime_action = self.q1_table.loc[s_prime, :]
                max_action_q1 = np.random.choice(q1_state_prime_action[q1_state_prime_action == np.max(q1_state_prime_action)].index)
                old_q1 = self.q1_table.loc[s, a]
                new_q1 = old_q1 + self.alpha*(r + (self.gamma*self.q2_table.loc[s_prime, max_action_q1]) - old_q1)
            else:
                new_q1 = r  # next state is terminal

            self.q1_table.loc[s, a] = new_q1  # update
        else:
            if s_prime != 'terminal':
                q2_state_prime_action = self.q2_table.loc[s_prime, :]
                max_action_q2 = np.random.choice(q2_state_prime_action[q2_state_prime_action == np.max(q2_state_prime_action)].index)
                old_q2 = self.q2_table.loc[s, a]
                new_q2 = old_q2 + self.alpha*(r + (self.gamma*self.q1_table.loc[s_prime, max_action_q2]) - old_q2)
            else:
                new_q2 = r  # next state is terminal

            self.q2_table.loc[s, a] = new_q2  # update
        
        a_prime = self.choose_action(s_prime)
        return s_prime, a_prime

    def check_state_exist(self, state):
        if state not in self.q1_table.index:
            # append new state to q table
            self.q1_table = self.q1_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q1_table.columns,
                    name=state,
                )
            )

        if state not in self.q2_table.index:
            # append new state to q table
            self.q2_table = self.q2_table.append(
                pd.Series(
                    [0]*len(self.actions),
                    index=self.q2_table.columns,
                    name=state,
                )
            )